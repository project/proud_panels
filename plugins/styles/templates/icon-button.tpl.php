<div class="card-wrap"><a href="<?php print $link ?>" class="card text-center card-btn card-block">
  <?php if(!empty($icon)): ?><i class="fa <?php print $icon; ?> fa-3x"></i><?php endif; ?>
  <?php print render($content); ?>
</a></div>