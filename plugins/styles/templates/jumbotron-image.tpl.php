<div <?php print drupal_attributes($img_attributes); ?>>
  <div class="container">
    <div <?php print drupal_attributes($wrapper_attributes); ?>>
      <div <?php print drupal_attributes($style_attributes); ?>>
        <div class="row">
          <div class="col-lg-7 col-md-8 col-sm-9">
            <div class="jumbotron-bg">
              <?php print render($content); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>